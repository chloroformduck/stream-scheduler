import os
import yaml
from dateutil.parser import parse
from dateutil.tz import gettz

tzinfo = {
    "EST": gettz("US/Eastern"),
    "PST": gettz("US/Pacific"),
    "MST": gettz("US/Mountain"),
    "CST": gettz("US/Central"),
    "CET": gettz("Europe/Berlin"),
    "JST": gettz("Asia/Tokyo")
    }

def startup():
    os.system('cls')
    print("Python 3.7.9 (tags/vDuck) [MSC v.1900 64 bit (AMD64)] on DuckOS")
    print('Type "help", "copyright", "credits" or "license" for more information.')
    print(">>> import stream")
    print(">>> stream.load_schedule()\n")

def main():

    with open("sched.yml", "r") as f:
        for data in yaml.safe_load_all(f):
            local_time = parse(data['date'], tzinfos=tzinfo)
            est_time = local_time.astimezone(gettz('US/Eastern'))
            europe_time = local_time.astimezone(gettz('Europe/Berlin'))
            japan_time = local_time.astimezone(gettz('Asia/Tokyo'))
            times = [est_time, europe_time, japan_time]
            print(f"{data['name']}:")
            print(f"    {local_time.strftime('%A, %B %d at %I:%M%p %Z')}\n")
            for time in times:
                if time.tzinfo != local_time.tzinfo:
                    print(f"    {time.strftime('%A, %B %d at %I:%M%p %Z')}\n")

startup()
main()