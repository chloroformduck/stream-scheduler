# Stream Scheduler

Easily generate your stream schedule from a template file  
***
Requirements:  
* [Git](https://git-scm.com/)
* [Python 3.7.9 or above](https://www.python.org/downloads/release/python-379/)
* A terminal of some kind (bash, powershell, cmd prompt)
***
## Download and use  

* First, use git to clone the repository to your local computer:  
```powershell
git clone https://gitlab.com/chloroformduck/stream-scheduler.git
```

* Next, `cd` into the directory:
```powershell
cd stream-scheduler
```

* Create a virtual environment for the script and activate it (optional but recommended):
```powershell
python -m venv .
./Scripts/Activate.ps1
```

* Install the required external python libraries from the requirements.txt file:
```powershell
pip install -r requirements.txt
```

* Modify the `sched.yml` file to your liking:
```yaml
---
name: "Testing my script"
date: "Jan 4 4 PM EST"
---
name: "Writing some code"
date: "Jan 6 6 PM EST"
```

* Run the script:
```powershell
python schedule.py
```

* Your schedule will be output and formatted for you:
```text
Python 3.7.9 (tags/vDuck) [MSC v.1900 64 bit (AMD64)] on DuckOS       
Type "help", "copyright", "credits" or "license" for more information.
>>> import stream
>>> stream.load_schedule()

Testing my script:
    Tuesday, January 04 at 04:00PM EST

    Tuesday, January 04 at 10:00PM CET

    Wednesday, January 05 at 06:00AM JST

Writing some code:
    Thursday, January 06 at 06:00PM EST

    Friday, January 07 at 12:00AM CET

    Friday, January 07 at 08:00AM JST
```
### Customize
If you'd prefer to customize the text at the top or completely remove it, modify the print statements under the startup function on [line 15 of schedule.py](schedule.py#15)

If you completely remove the function declaration, make sure to also remove the reference to it on [line 37](schedule.py#37)

***
## Issues/Feedback
If you come accross any issues or have any feedback, feel free to [open an issue](https://gitlab.com/chloroformduck/stream-scheduler/-/issues/new) here on gitlab or reach out to me on [Twitter](https://twitter.com/chloroformduck1)